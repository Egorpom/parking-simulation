package ru.ssau.parkingsimulation.simulation;

import ru.ssau.parkingsimulation.config.Configuration;
import ru.ssau.parkingsimulation.config.setting.DoubleSetting;
import ru.ssau.parkingsimulation.config.setting.EnumSetting;
import ru.ssau.parkingsimulation.simulation.generator.DistributionGenerator;
import ru.ssau.parkingsimulation.simulation.generator.DistributionLaw;
import ru.ssau.parkingsimulation.simulation.tariff.TariffConfig;

import java.util.Objects;

public class SimulationConfig extends Configuration {
    private DoubleSetting parkingEntryProbability = new DoubleSetting("Вероятность заезда на парковку", 0.5);
    private DoubleSetting truckSpawnProbability = new DoubleSetting("Вероятность появления грузовой машины", 0.5);
    private EnumSetting<DistributionLaw> distributionLaw = new EnumSetting<>("Тип генерации потока машин", DistributionLaw.values());
    private DistributionGenerator generator = distributionLaw.getValue().getGenerator();
    private TariffConfig tariffConfig = new TariffConfig();

    public SimulationConfig() {
        super("Симуляция");
    }

    public DistributionLaw getDistributionLaw() {
        return distributionLaw.getValue();
    }

    public double getParkingEntryProbability() {
        return parkingEntryProbability.getValue();
    }

    public double getTruckSpawnProbability() {
        return truckSpawnProbability.getValue();
    }

    public TariffConfig getTariffConfig() {
        return tariffConfig;
    }

    @Override
    public boolean isValid() {
        return super.isValid()
                && parkingEntryProbability.getValue() >= 0.0D
                && parkingEntryProbability.getValue() <= 1.0D
                && truckSpawnProbability.getValue() >= 0.0D
                && truckSpawnProbability.getValue() <= 1.0D
                && generator.isValid()
                && tariffConfig.isValid();
    }

    @Override
    public void updateSource() {
        super.updateSource();
        SimulationConfig simulationConfig = (SimulationConfig) source;
        simulationConfig.parkingEntryProbability.setValue(parkingEntryProbability.getValue());
        simulationConfig.truckSpawnProbability.setValue(truckSpawnProbability.getValue());
        simulationConfig.distributionLaw.setValue(distributionLaw.getValue());
        simulationConfig.generator = distributionLaw.getValue().getGenerator();
        generator.updateSource();
        tariffConfig.updateSource();
    }

    @Override
    public void buildTreeItem() {
        super.buildTreeItem();
        treeItem.getChildren().add(tariffConfig.getTreeItem());
    }

    @Override
    public void buildNode() {
        super.buildNode();
        node.getChildren().addAll(
                parkingEntryProbability.getNode(),
                truckSpawnProbability.getNode(),
                distributionLaw.getNode(),
                generator.getNode()
        );
        distributionLaw.observableProperty().addListener((observable, oldValue, newValue) -> {
            node.getChildren().remove(generator.getNode());
            generator = (DistributionGenerator) newValue.getGenerator().clone();
            node.getChildren().add(generator.getNode());
        });
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SimulationConfig)) return false;
        if (!super.equals(o)) return false;
        SimulationConfig that = (SimulationConfig) o;
        return parkingEntryProbability.equals(that.parkingEntryProbability)
                && truckSpawnProbability.equals(that.truckSpawnProbability)
                && distributionLaw.equals(that.distributionLaw)
                && generator.equals(that.generator)
                && tariffConfig.equals(that.tariffConfig);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), parkingEntryProbability, truckSpawnProbability, distributionLaw, generator, tariffConfig);
    }

    @Override
    public SimulationConfig clone() {
        SimulationConfig config = (SimulationConfig) super.clone();
        config.parkingEntryProbability = (DoubleSetting) parkingEntryProbability.clone();
        config.truckSpawnProbability = (DoubleSetting) truckSpawnProbability.clone();
        config.distributionLaw = (EnumSetting<DistributionLaw>) distributionLaw.clone();
        config.generator = (DistributionGenerator) generator.clone();
        config.tariffConfig = tariffConfig.clone();
        return config;
    }
}