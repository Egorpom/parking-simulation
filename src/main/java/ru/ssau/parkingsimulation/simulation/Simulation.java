package ru.ssau.parkingsimulation.simulation;

import javafx.animation.*;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.shape.HLineTo;
import javafx.scene.shape.Path;
import javafx.util.Duration;
import ru.ssau.parkingsimulation.model.Car;
import ru.ssau.parkingsimulation.model.CarType;
import ru.ssau.parkingsimulation.model.Model;
import ru.ssau.parkingsimulation.model.construction.Construction;
import ru.ssau.parkingsimulation.util.Constants;
import ru.ssau.parkingsimulation.view.controller.MainController;

import java.util.Random;
import java.util.Set;
import java.util.concurrent.*;

public class Simulation {
    public static Simulation INSTANCE = new Simulation();
    protected SimulationConfig config = new SimulationConfig();
    private SimpleObjectProperty<SimulationState> state = new SimpleObjectProperty<>(SimulationState.STOPPED);
    private Set<Car> cars = ConcurrentHashMap.newKeySet();
    private ExecutorService executorService = Executors.newSingleThreadExecutor();
    private Future<?> carGenerator;
    private DoubleBinding speedRate;
    private TimeType timeType = Constants.TIME_TYPE_DEFAULT;
    private Timeline realTimeline;
    private Timeline modelTimeline;

    private Simulation() {
        SimpleObjectProperty<Double> originalSpeedRate = SimulationToolbar.INSTANCE.getSpeedRateObservable();
        speedRate = Bindings.createDoubleBinding(new Callable<Double>() {
            private double lastNotPaused;

            @Override
            public Double call() {
                if (state.getValue() != SimulationState.PAUSED) {
                    return lastNotPaused = originalSpeedRate.getValue();
                }
                return lastNotPaused;
            }
        }, state, originalSpeedRate);
        realTimeline = new Timeline(new KeyFrame(Duration.seconds(1), event -> SimulationToolbar.INSTANCE.incrementTime()));
        realTimeline.setCycleCount(Timeline.INDEFINITE);
        modelTimeline = createTimeLine();
    }

    public SimulationConfig getConfig() {
        return config;
    }

    public boolean isActive() {
        return state.getValue() != SimulationState.STOPPED;
    }

    public SimulationState getState() {
        return state.getValue();
    }

    public SimpleObjectProperty<SimulationState> stateProperty() {
        return state;
    }

    public TimeType getTimeType() {
        return timeType;
    }

    public void start() {
        realTimeline.play();
        modelTimeline.play();
        if (getState() == SimulationState.PAUSED) {
            for (Car car : cars) {
                car.getAnimation().play();
            }
            state.setValue(SimulationState.STARTED);
        } else {
            Model.INSTANCE.refreshCache();
            state.setValue(SimulationState.STARTED);
            carGenerator = executorService.submit(new CarGenerator());
        }
    }

    public void pause() {
        realTimeline.pause();
        modelTimeline.pause();
        for (Car car : cars) {
            car.getAnimation().pause();
        }
        state.setValue(SimulationState.PAUSED);
    }

    public void restart() {
        stop();
        start();
    }

    public void stop() {
        carGenerator.cancel(true);
        state.setValue(SimulationState.STOPPED);
        realTimeline.stop();
        modelTimeline.stop();
        for (Car car : cars) {
            car.getAnimation().stop();
            MainController.mainPane.getChildren().remove(car.getNode());
        }
        cars.clear();
        for (Construction value : Model.INSTANCE.getConstructions().values()) {
            value.setBusy(false);
        }
        timeType = TimeType.DAY;
        SimulationToolbar.INSTANCE.clear();
    }

    private Timeline createTimeLine() {
        TimeType[] timeTypes = TimeType.values();
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(1), event -> {
            SimulationToolbar.INSTANCE.incrementModelTime();
            if (SimulationToolbar.INSTANCE.getModelTime() % Constants.TIME_BEFORE_TIME_TYPE_SWITCH_SEC == 0) {
                timeType = timeTypes[(timeType.ordinal() + 1) % timeTypes.length];
                SimulationToolbar.INSTANCE.setTimeType(timeType);
            }
        }));
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.rateProperty().bind(speedRate);
        return timeline;
    }

    private class CarGenerator implements Runnable {
        @Override
        public void run() {
            Random random = new Random();
            while (isActive()) {
                try {
                    if (getState() == SimulationState.STARTED) {
                        Platform.runLater(() -> {
                            boolean isTruck = random.nextDouble() <= config.getTruckSpawnProbability();
                            boolean entryToParking = random.nextDouble() <= config.getParkingEntryProbability();
                            Car car = new Car(isTruck ? CarType.TRUCK : CarType.CAR);
                            MainController.mainPane.getChildren().add(car.getNode());
                            SequentialTransition animation = new SequentialTransition();
                            animation.getChildren().addAll(PathManager.createPath(car, entryToParking, generateParkingTime(random.nextDouble())));
                            animation.setNode(car.getNode());
                            car.setAnimation(animation);
                            cars.add(car);
                            animation.setOnFinished(event -> {
                                cars.remove(car);
                                MainController.mainPane.getChildren().remove(car.getNode());
                            });
                            animation.setInterpolator(Interpolator.LINEAR);
                            //TODO Refactor
                            double timeRate = getTimeRate(animation) / 500.0;
                            animation.rateProperty().bind(speedRate.divide(timeRate));
                            animation.play();
                        });
                    }
                    Thread.sleep((long) (config.getDistributionLaw().getGenerator().nextValue() * 1000 / speedRate.getValue()));
                } catch (InterruptedException ignored) {
                }
            }
        }

        private Duration generateParkingTime(double random) {
            return Duration.seconds((Constants.TIME_MAX_CAR_PARKING - Constants.TIME_MIN_CAR_PARKING) * random + Constants.TIME_MIN_CAR_PARKING);
        }

        private double getTimeRate(SequentialTransition animation) {
            PathTransition transition = (PathTransition) animation.getChildren().get(0);
            Duration totalDuration = transition.getTotalDuration();
            DoubleProperty xProperty = ((HLineTo) ((Path) transition.getPath()).getElements().get(1)).xProperty();
            return MainController.mainPane.widthProperty().subtract(xProperty).getValue() / totalDuration.toSeconds();
        }
    }
}