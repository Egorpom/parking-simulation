package ru.ssau.parkingsimulation.simulation;

import javafx.animation.*;
import javafx.beans.binding.DoubleExpression;
import javafx.scene.layout.GridPane;
import javafx.scene.shape.*;
import javafx.util.Duration;
import ru.ssau.parkingsimulation.model.Car;
import ru.ssau.parkingsimulation.model.Model;
import ru.ssau.parkingsimulation.model.Road;
import ru.ssau.parkingsimulation.model.construction.*;
import ru.ssau.parkingsimulation.simulation.checker.ConstructionChecker;
import ru.ssau.parkingsimulation.simulation.checker.ExistsChecker;
import ru.ssau.parkingsimulation.simulation.checker.ParkPathChecker;
import ru.ssau.parkingsimulation.view.controller.MainController;
import ru.ssau.parkingsimulation.view.pane.ConstructionPane;

import java.util.*;

public class PathManager {
    private PathManager() {
    }

    public static boolean isPathsExists(Construction construction, IConstructionType... to) {
        for (IConstructionType type : to) {
            if (!isPathExists(construction, type)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isPathExists(Construction construction, IConstructionType to) {
        return !findPath(construction, new ExistsChecker(to)).isEmpty();
    }

    public static List<Direction> findPath(Construction construction, ConstructionChecker checker) {
        List<Direction> path = new ArrayList<>();
        findPath(construction, checker, path, new HashSet<>(), 0);
        return path;
    }

    public static boolean findPath(Construction construction, ConstructionChecker checker, List<Direction> path, Set<Position> visitedPositions, int level) {
        Position position = construction.getPosition();
        visitedPositions.add(position);
        if (checker.isValid(construction)) {
            if (level == 1) {
                visitedPositions.remove(position);
                return false;
            }
            return true;
        }
        if (level != 0 && construction.getType() != ConstructionType.ROAD) {
            return false;
        }
        for (Direction direction : Direction.values()) {
            Position nextPosition = position.applyDirection(direction);
            Construction nextConstruction = Model.INSTANCE.getConstruction(nextPosition);
            if (visitedPositions.contains(nextPosition) || nextConstruction == null) {
                continue;
            }
            if (findPath(nextConstruction, checker, path, visitedPositions, level + 1)) {
                path.add(0, direction);
                return true;
            }
        }
        return false;
    }

    public static List<Transition> createPath(Car car, boolean entryToParking, Duration pauseDuration) {
        if (entryToParking) {
            List<Direction> pathToPark = findPath(Model.INSTANCE.getConstructionFromCache(ConstructionType.ENTRY), new ParkPathChecker(car));
            if (!pathToPark.isEmpty()) {
                List<Transition> transitions = new ArrayList<>();
                //Null -> Start
                List<Transition> start = createStart(car);
                Construction currentCons = Model.INSTANCE.getConstructionFromCache(ConstructionType.ENTRY);
                //Start -> Park
                List<Transition> parking = translatePath(car, pathToPark);
                final Construction parkingConstruction = getConstruction(currentCons, pathToPark);
                currentCons = parkingConstruction;
                //Pause
                PauseTransition pause = new PauseTransition(pauseDuration);
                //Park -> Pay
                List<Direction> pathToPay = findPath(currentCons, new ExistsChecker(ConstructionType.PAYMENT_BOOTH));
                //Remove last because car can't entry to pay
                pathToPay.remove(pathToPay.size() - 1);
                List<Transition> pay = translatePath(car, pathToPay);
                //Pay -> Exit
                List<Direction> pathToExit = findPath(getConstruction(currentCons, pathToPay), new ExistsChecker(ConstructionType.EXIT));
                List<Transition> exit = translatePath(car, pathToExit);
                //Exit -> End
                transitions.addAll(start);
                transitions.addAll(parking);
                transitions.add(pause);
                transitions.add(createRotation(getLast(pathToPark), pathToPay.get(0)));
                transitions.addAll(pay);
                transitions.add(createRotation(getLast(pathToPay), pathToExit.get(0)));
                transitions.addAll(exit);
                final List<Transition> allInParkTransitions = new ArrayList<>(transitions.subList(start.size(), transitions.size()));
                transitions.add(translatePathPart(car, Direction.DOWN, 1));
                transitions.add(createRotation(Direction.DOWN, Direction.LEFT));
                transitions.add(createEnd(car));
                //Bind actions
                //Double set busy. First for lock place, second - event about real entry
                parkingConstruction.setBusy(true);
                getLast(start).setOnFinished(event -> {
                    parkingConstruction.setParkingTime(car, collectTotalDuration(allInParkTransitions));
                    //parkingConstruction.setParkingTime(car, pauseDuration);
                    parkingConstruction.setBusy(true);
                });
                getLast(pay).setOnFinished(event -> SimulationToolbar.INSTANCE.addProfit(parkingConstruction.getPayment().getValue()));
                getLast(exit).setOnFinished(event -> parkingConstruction.setBusy(false));
                return transitions;
            }
        }
        return Collections.singletonList(createDefaultPath(car));
    }

    /********************************************************************
     *  Transitions
     ********************************************************************/
    //Start to End
    public static PathTransition createDefaultPath(Car car) {
        PathTransition transition = createPathTransition();
        Path path = new Path(createStartPoint(car), createEndPoint(car));
        transition.setPath(path);
        transition.setDuration(Duration.seconds(2));
        return transition;
    }

    public static List<Transition> createStart(Car car) {
        List<Transition> transitions = new ArrayList<>();
        PathTransition pathTransition = createPathTransition();
        pathTransition.setPath(new Path(createStartPoint(car), createLineToEntry()));
        transitions.add(pathTransition);
        transitions.add(createRotation(Direction.LEFT, Direction.UP));
        transitions.add(translatePathPart(car, Direction.UP, 1));
        return transitions;
    }

    public static List<Transition> translatePath(Car car, List<Direction> path) {
        List<Transition> transitions = new ArrayList<>();
        Direction prevDirection = null;
        int countSame = 0;
        for (Direction direction : path) {
            if (prevDirection == direction) {
                ++countSame;
            } else if (prevDirection != null) {
                transitions.add(translatePathPart(car, prevDirection, countSame + 1));
                transitions.add(createRotation(prevDirection, direction));
                countSame = 0;
            }
            prevDirection = direction;
        }
        transitions.add(translatePathPart(car, prevDirection, countSame + 1));
        return transitions;
    }

    public static PathTransition translatePathPart(Car car, Direction direction, int length) {
        PathTransition pathTransition = createPathTransition();
        pathTransition.setPath(new Path(createAbsolutePoint(car), createLinearPathElement(direction, length)));
        return pathTransition;
    }

    public static PathTransition createEnd(Car car) {
        PathTransition transition = createPathTransition();
        transition.setPath(new Path(createAbsolutePoint(car), createEndPoint(car)));
        return transition;
    }

    public static RotateTransition createRotation(Direction from, Direction to) {
        int length = Direction.values().length;
        int normal = (length + to.ordinal() - from.ordinal()) % length;
        int reverse = (length - normal) % length;
        double rotation = 90.0;
        if (normal <= reverse) {
            rotation *= normal;
        } else {
            rotation *= -reverse;
        }
        RotateTransition transition = createRotateTransition();
        transition.setByAngle(rotation);
        return transition;
    }

    public static RotateTransition createRotateTransition() {
        RotateTransition transition = new RotateTransition(Duration.seconds(0.5));
        transition.setInterpolator(Interpolator.LINEAR);
        return transition;
    }

    public static PathTransition createPathTransition() {
        PathTransition transition = new PathTransition();
        transition.setInterpolator(Interpolator.LINEAR);
        transition.setDuration(Duration.seconds(1.5));
        return transition;
    }

    /********************************************************************
     *  PathElements
     ********************************************************************/
    public static MoveTo createStartPoint(Car car) {
        MoveTo moveTo = new MoveTo();
        moveTo.xProperty().bind(MainController.mainPane.widthProperty().add(car.getNode().widthProperty()));
        moveTo.yProperty().bind(MainController.mainPane.heightProperty().subtract(Road.INSTANCE.getNode().heightProperty().divide(2)));
        return moveTo;
    }

    public static HLineTo createLineToEntry() {
        Model model = Model.INSTANCE;
        ConstructionPane entryPane = model.getConstructionFromCache(ConstructionType.ENTRY).getNode();
        HLineTo line = new HLineTo();
        //Root - (Road - Model) / 2 - (Width - Index - 0.5) * Cons
        line.xProperty().bind(MainController.mainPane.widthProperty()
                .subtract(Road.INSTANCE.getNode().widthProperty().subtract(model.getModelPane().widthProperty()).divide(2))
                .subtract(entryPane.widthProperty().multiply(model.getConfig().getWidth() - GridPane.getColumnIndex(entryPane) - 0.5))
        );
        return line;
    }

    public static PathElement createLinearPathElement(Direction direction, int length) {
        PathElement pathElement;
        ConstructionPane consPane = Model.INSTANCE.getConstructionFromCache(ConstructionType.ENTRY).getNode();
        DoubleExpression sideSize = consPane.widthProperty().multiply(length);
        if (direction == Direction.UP || direction == Direction.LEFT) {
            sideSize = sideSize.negate();
        }
        if (direction == Direction.UP || direction == Direction.DOWN) {
            VLineTo line = new VLineTo();
            line.yProperty().bind(sideSize);
            pathElement = line;
        } else {
            HLineTo line = new HLineTo();
            line.xProperty().bind(sideSize);
            pathElement = line;
        }
        pathElement.setAbsolute(false);
        return pathElement;
    }

    public static HLineTo createEndPoint(Car car) {
        HLineTo hLineTo = new HLineTo();
        hLineTo.xProperty().bind(MainController.mainPane.widthProperty()
                .subtract(Road.INSTANCE.getNode().widthProperty())
                .add(car.getNode().widthProperty().divide(2))
        );
        return hLineTo;
    }

    public static MoveTo createAbsolutePoint(Car car) {
        Rectangle carPane = car.getNode();
        MoveTo moveTo = new MoveTo();
        moveTo.xProperty().bind(carPane.translateXProperty().add(carPane.widthProperty().divide(2)));
        moveTo.yProperty().bind(carPane.translateYProperty().add(carPane.heightProperty().divide(2)));
        return moveTo;
    }

    public static Construction getConstruction(Construction start, List<Direction> directions) {
        return Model.INSTANCE.getConstruction(start.getPosition().applyDirections(directions));
    }

    private static <T> T getLast(List<T> list) {
        return list.get(list.size() - 1);
    }

    private static Duration collectTotalDuration(List<Transition> transitions) {
        Duration duration = Duration.ZERO;
        for (Transition transition : transitions) {
            duration = duration.add(transition.getTotalDuration());
        }
        return duration;
    }
}