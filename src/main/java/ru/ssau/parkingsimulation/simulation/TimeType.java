package ru.ssau.parkingsimulation.simulation;

public enum TimeType {
    DAY("День", "днем"), NIGHT("Ночь", "ночью");
    private String[] titles;

    TimeType(String... titles) {
        this.titles = titles;
    }

    public String getTitle() {
        return titles[1];
    }

    @Override
    public String toString() {
        return titles[0];
    }
}