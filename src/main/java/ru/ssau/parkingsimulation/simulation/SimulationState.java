package ru.ssau.parkingsimulation.simulation;

public enum SimulationState {
    STARTED,
    PAUSED,
    STOPPED
}