package ru.ssau.parkingsimulation.simulation.generator;

import javafx.geometry.Insets;
import ru.ssau.parkingsimulation.config.Configuration;
import ru.ssau.parkingsimulation.util.Constants;

import java.util.Objects;
import java.util.Random;

public abstract class DistributionGenerator extends Configuration {
    protected DistributionLaw law;
    protected Random random = new Random();

    public void setLaw(DistributionLaw law) {
        this.law = law;
    }

    public double nextValue() {
        return calculate(random.nextDouble());
    }

    public abstract double calculate(double random);

    @Override
    public boolean isValid() {
        return super.isValid()
                && calculate(0.0D) >= Constants.TIME_MIN_BETWEEN_CAR_SPAWN
                && calculate(1.0D) >= Constants.TIME_MIN_BETWEEN_CAR_SPAWN
                && calculate(0.0D) <= Constants.TIME_MAX_BETWEEN_CAR_SPAWN
                && calculate(1.0D) <= Constants.TIME_MAX_BETWEEN_CAR_SPAWN;
    }

    @Override
    public void buildNode() {
        super.buildNode();
        node.setPadding(Insets.EMPTY);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DistributionGenerator)) return false;
        if (!super.equals(o)) return false;
        DistributionGenerator that = (DistributionGenerator) o;
        return law == that.law;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), law);
    }
}