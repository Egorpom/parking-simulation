package ru.ssau.parkingsimulation.simulation.generator;

import ru.ssau.parkingsimulation.config.setting.DoubleSetting;
import ru.ssau.parkingsimulation.util.Constants;

import java.util.Objects;

public class UniformGenerator extends DistributionGenerator {
    private DoubleSetting a = new DoubleSetting("Начало промежутка (a)", Constants.TIME_MIN_BETWEEN_CAR_SPAWN);
    private DoubleSetting b = new DoubleSetting("Конец промежутка (b)", Constants.TIME_MAX_BETWEEN_CAR_SPAWN);

    @Override
    public double calculate(double random) {
        return (b.getValue() - a.getValue()) * random + a.getValue();
    }

    @Override
    public void updateSource() {
        super.updateSource();
        UniformGenerator generator = (UniformGenerator) source;
        generator.a.setValue(a.getValue());
        generator.b.setValue(b.getValue());
    }

    @Override
    public void buildNode() {
        super.buildNode();
        node.getChildren().addAll(a.getNode(), b.getNode());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UniformGenerator)) return false;
        if (!super.equals(o)) return false;
        UniformGenerator that = (UniformGenerator) o;
        return a.equals(that.a) && b.equals(that.b);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), a, b);
    }

    @Override
    public UniformGenerator clone() {
        UniformGenerator generator = (UniformGenerator) super.clone();
        generator.a = (DoubleSetting) a.clone();
        generator.b = (DoubleSetting) b.clone();
        return generator;
    }
}