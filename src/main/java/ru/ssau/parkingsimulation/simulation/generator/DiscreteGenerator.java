package ru.ssau.parkingsimulation.simulation.generator;

import ru.ssau.parkingsimulation.config.setting.DoubleSetting;
import ru.ssau.parkingsimulation.util.Constants;

import java.util.Objects;

public class DiscreteGenerator extends DistributionGenerator {
    private DoubleSetting time = new DoubleSetting("Время между появлением машин", Constants.TIME_MIN_BETWEEN_CAR_SPAWN);

    @Override
    public double calculate(double random) {
        return time.getValue();
    }

    @Override
    public void updateSource() {
        DiscreteGenerator generator = (DiscreteGenerator) source;
        generator.time.setValue(time.getValue());
    }

    @Override
    public void buildNode() {
        super.buildNode();
        node.getChildren().add(time.getNode());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DiscreteGenerator)) return false;
        if (!super.equals(o)) return false;
        DiscreteGenerator that = (DiscreteGenerator) o;
        return time.equals(that.time);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), time);
    }

    @Override
    public DiscreteGenerator clone() {
        DiscreteGenerator configuration = (DiscreteGenerator) super.clone();
        configuration.time = (DoubleSetting) time.clone();
        return configuration;
    }
}