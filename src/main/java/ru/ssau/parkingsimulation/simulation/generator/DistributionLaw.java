package ru.ssau.parkingsimulation.simulation.generator;

public enum DistributionLaw {
    DISCRETE("Дискретный", new DiscreteGenerator()),
    UNIFORM("Равномерный", new UniformGenerator()),
    EXPONENTIAL("Показательный", new ExponentialGenerator()),
    NORMAL("Нормальный", new NormalGenerator());

    private String title;
    private DistributionGenerator generator;

    DistributionLaw(String title, DistributionGenerator generator) {
        this.title = title;
        this.generator = generator;
        this.generator.setLaw(this);
    }

    public DistributionGenerator getGenerator() {
        return generator;
    }

    @Override
    public String toString() {
        return title;
    }
}