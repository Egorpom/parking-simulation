package ru.ssau.parkingsimulation.simulation.generator;

import ru.ssau.parkingsimulation.config.Configuration;
import ru.ssau.parkingsimulation.config.setting.DoubleSetting;

import java.util.Objects;

public class ExponentialGenerator extends DistributionGenerator {
    private DoubleSetting lambda = new DoubleSetting("Интенсивность (λ)", 0.5D);

    @Override
    public double calculate(double random) {
        if (random < 0.01D) {
            random = 0.01D;
        } else if (random > 0.6D) {
            random = 0.6D;
        }
        return -Math.log(random) / lambda.getValue();
    }

    @Override
    public void updateSource() {
        super.updateSource();
        ExponentialGenerator generator = (ExponentialGenerator) source;
        generator.lambda.setValue(lambda.getValue());
    }

    @Override
    public void buildNode() {
        super.buildNode();
        node.getChildren().add(lambda.getNode());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ExponentialGenerator)) return false;
        if (!super.equals(o)) return false;
        ExponentialGenerator that = (ExponentialGenerator) o;
        return lambda.equals(that.lambda);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), lambda);
    }

    @Override
    public Configuration clone() {
        ExponentialGenerator generator = (ExponentialGenerator) super.clone();
        generator.lambda = (DoubleSetting) lambda.clone();
        return generator;
    }
}