package ru.ssau.parkingsimulation.simulation.generator;

import ru.ssau.parkingsimulation.config.Configuration;
import ru.ssau.parkingsimulation.config.setting.DoubleSetting;

import java.util.Objects;

public class NormalGenerator extends DistributionGenerator {
    private DoubleSetting d = new DoubleSetting("Дисперсия (σ)", 196.0D);
    private DoubleSetting m = new DoubleSetting("Мат. ожидание (α)", 1.0D);

    @Override
    public double nextValue() {
        return calculate(random.nextGaussian());
    }

    @Override
    public double calculate(double random) {
        return random * Math.sqrt(d.getValue()) + m.getValue();
    }

    @Override
    public void updateSource() {
        super.updateSource();
        NormalGenerator generator = (NormalGenerator) source;
        generator.d.setValue(d.getValue());
        generator.m.setValue(m.getValue());
    }

    @Override
    public void buildNode() {
        super.buildNode();
        node.getChildren().addAll(d.getNode(), m.getNode());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NormalGenerator)) return false;
        if (!super.equals(o)) return false;
        NormalGenerator that = (NormalGenerator) o;
        return m.equals(that.m) && d.equals(that.d);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), m, d);
    }

    @Override
    public Configuration clone() {
        NormalGenerator generator = (NormalGenerator) super.clone();
        generator.d = (DoubleSetting) d.clone();
        generator.m = (DoubleSetting) m.clone();
        return generator;
    }
}