package ru.ssau.parkingsimulation.simulation;

import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import ru.ssau.parkingsimulation.config.setting.DoubleSetting;
import ru.ssau.parkingsimulation.config.setting.EnumSetting;
import ru.ssau.parkingsimulation.config.setting.IntegerSetting;
import ru.ssau.parkingsimulation.config.setting.StringSetting;
import ru.ssau.parkingsimulation.util.Constants;
import ru.ssau.parkingsimulation.view.INode;
import ru.ssau.parkingsimulation.view.ViewUtils;

public class SimulationToolbar implements INode {
    public static SimulationToolbar INSTANCE = new SimulationToolbar();
    private boolean simMode;
    private SimpleLongProperty time = new SimpleLongProperty(0);
    private SimpleLongProperty modelTime = new SimpleLongProperty(0);
    private StringSetting timeSetting = new StringSetting("Время", "", true);
    private StringSetting modelTimeSetting = new StringSetting("Модельное время", "", true);
    private DoubleSetting speedRate = new DoubleSetting("Скорость", 1.0D, true);
    private IntegerSetting profit = new IntegerSetting("Прибыль", 0, true);
    private EnumSetting<TimeType> timeTypeSetting = new EnumSetting<>("Время суток", TimeType.values(), Constants.TIME_TYPE_DEFAULT, true);
    protected VBox node;
    private Button[] buttons = new Button[4];

    private SimulationToolbar() {
        timeSetting.observableProperty().bind(Bindings.createStringBinding(() -> ViewUtils.formatRealTime(time.getValue()), time));
        modelTimeSetting.observableProperty().bind(Bindings.createStringBinding(() -> ViewUtils.formatModelTime(modelTime.getValue()), modelTime));
    }

    public boolean isSimMode() {
        return simMode;
    }

    public void setSimMode(boolean simMode) {
        this.simMode = simMode;
    }

    public Button[] getButtons() {
        return buttons;
    }

    public SimpleObjectProperty<Double> getSpeedRateObservable() {
        return speedRate.observableProperty();
    }

    public void incrementTime() {
        time.setValue(time.getValue() + 1);
    }

    public long getTime() {
        return time.getValue();
    }

    public void incrementModelTime() {
        modelTime.setValue(modelTime.getValue() + 1);
    }

    public long getModelTime() {
        return modelTime.getValue();
    }

    public SimpleObjectProperty<TimeType> getTimeTypeObservable() {
        return timeTypeSetting.observableProperty();
    }

    public void setTimeType(TimeType timeType) {
        timeTypeSetting.setValue(timeType);
    }

    public void addProfit(Integer profit) {
        this.profit.setValue(this.profit.getValue() + profit);
    }

    public void clear() {
        profit.setValue(0);
        time.setValue(0);
        modelTime.setValue(0);
    }

    @Override
    public VBox getNode() {
        if (node == null) {
            buildNode();
        }
        return node;
    }

    @Override
    public void buildNode() {
        node = new VBox();
        node.setSpacing(10);
        HBox hBoxButtons = new HBox();
        hBoxButtons.setSpacing(1);
        buttons[0] = new Button("Старт");
        buttons[1] = new Button("Пауза");
        buttons[2] = new Button("Перезапуск");
        buttons[3] = new Button("Стоп");
        hBoxButtons.getChildren().addAll(buttons);
        Slider slider = new Slider(Constants.SPEED_RATE_MIN, Constants.SPEED_RATE_MAX, 1);
        slider.setShowTickLabels(true);
        slider.setShowTickMarks(true);
        slider.setSnapToTicks(true);
        slider.setBlockIncrement(0.25);
        slider.setMinorTickCount(0);
        slider.setMajorTickUnit(0.25);
        speedRate.setValue(slider.getValue());
        slider.valueProperty().addListener((observable, oldValue, newValue) -> speedRate.setValue(Math.rint((Double) newValue * 100) / 100));
        node.getChildren().addAll(timeSetting.getNode(), modelTimeSetting.getNode(), speedRate.getNode(), profit.getNode(), timeTypeSetting.getNode(), hBoxButtons, slider);
    }
}