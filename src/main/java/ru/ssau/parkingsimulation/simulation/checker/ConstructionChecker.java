package ru.ssau.parkingsimulation.simulation.checker;

import ru.ssau.parkingsimulation.model.construction.Construction;

public interface ConstructionChecker {
    boolean isValid(Construction construction);
}