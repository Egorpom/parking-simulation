package ru.ssau.parkingsimulation.simulation.checker;

import ru.ssau.parkingsimulation.model.Car;
import ru.ssau.parkingsimulation.model.CarType;
import ru.ssau.parkingsimulation.model.construction.Construction;
import ru.ssau.parkingsimulation.model.construction.ConstructionType;

public class ParkPathChecker implements ConstructionChecker {
    protected Car car;

    public ParkPathChecker(Car car) {
        this.car = car;
    }

    @Override
    public boolean isValid(Construction construction) {
        return !construction.isBusy()
                && ((construction.getType() == ConstructionType.PARKING_PLACE_CAR && car.getType() == CarType.CAR)
                || (construction.getType() == ConstructionType.PARKING_PLACE_TRUCK && car.getType() == CarType.TRUCK));
    }
}