package ru.ssau.parkingsimulation.simulation.checker;

import ru.ssau.parkingsimulation.model.construction.Construction;
import ru.ssau.parkingsimulation.model.construction.IConstructionType;

public class ExistsChecker implements ConstructionChecker {
    protected IConstructionType type;

    public ExistsChecker(IConstructionType type) {
        this.type = type;
    }

    @Override
    public boolean isValid(Construction construction) {
        return construction.getType() == type;
    }
}