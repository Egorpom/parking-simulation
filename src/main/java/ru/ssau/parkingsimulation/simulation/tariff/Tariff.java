package ru.ssau.parkingsimulation.simulation.tariff;

import com.google.gson.*;
import ru.ssau.parkingsimulation.config.setting.IntegerSetting;
import ru.ssau.parkingsimulation.model.CarType;
import ru.ssau.parkingsimulation.simulation.TimeType;
import ru.ssau.parkingsimulation.util.Constants;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Objects;

public class Tariff implements Serializable, Cloneable {
    private CarType carType;
    private TimeType timeType;
    private IntegerSetting price;

    public Tariff(CarType carType, TimeType timeType) {
        this(carType, timeType, Constants.TAX_MIN_PRICE);
    }

    public Tariff(CarType carType, TimeType timeType, int price) {
        this.carType = carType;
        this.timeType = timeType;
        this.price = new IntegerSetting(formatTitle(carType, timeType), price);
    }

    public CarType getCarType() {
        return carType;
    }

    public TimeType getTimeType() {
        return timeType;
    }

    public Integer getPrice() {
        return price.getValue();
    }

    public void setPrice(Integer price) {
        this.price.setValue(price);
    }

    public IntegerSetting getPriceSetting() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tariff)) return false;
        Tariff tariff = (Tariff) o;
        return carType == tariff.carType && timeType == tariff.timeType && price.equals(tariff.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(carType, timeType, price);
    }

    @Override
    public Tariff clone() {
        Tariff tariff = null;
        try {
            tariff = (Tariff) super.clone();
            tariff.price = (IntegerSetting) price.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return tariff;
    }

    public static String formatTitle(CarType carType, TimeType timeType) {
        return String.format("Цена за стоянку %s в единицу времени для %s автомобиля", timeType.getTitle(), carType.getTitle());
    }

    public static class JsonAdapter implements JsonSerializer<Tariff>, JsonDeserializer<Tariff> {
        @Override
        public JsonElement serialize(Tariff src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add("carType", context.serialize(src.carType));
            jsonObject.add("timeType", context.serialize(src.timeType));
            jsonObject.addProperty("price", src.price.getValue());
            return jsonObject;
        }

        @Override
        public Tariff deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject jsonObject = json.getAsJsonObject();
            CarType carType = context.deserialize(jsonObject.get("carType"), CarType.class);
            TimeType timeType = context.deserialize(jsonObject.get("timeType"), TimeType.class);
            int price = jsonObject.get("price").getAsInt();
            return new Tariff(carType, timeType, price);
        }
    }
}