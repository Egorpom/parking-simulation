package ru.ssau.parkingsimulation.simulation.tariff;

import javafx.collections.ObservableList;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import ru.ssau.parkingsimulation.config.Configuration;
import ru.ssau.parkingsimulation.model.Car;
import ru.ssau.parkingsimulation.model.CarType;
import ru.ssau.parkingsimulation.simulation.Simulation;
import ru.ssau.parkingsimulation.simulation.TimeType;
import ru.ssau.parkingsimulation.util.Constants;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class TariffConfig extends Configuration {
    private List<Tariff> tariffs = new ArrayList<>();

    public TariffConfig() {
        super("Тарифы");
        for (CarType carType : CarType.values()) {
            for (TimeType timeType : TimeType.values()) {
                tariffs.add(new Tariff(carType, timeType));
            }
        }
    }

    public Tariff getActiveTariff(Car car) {
        for (Tariff tariff : tariffs) {
            if (tariff.getCarType() == car.getType() && Simulation.INSTANCE.getTimeType() == tariff.getTimeType()) {
                return tariff;
            }
        }
        return null;
    }

    @Override
    public boolean isValid() {
        boolean isValid = super.isValid();
        for (Iterator<Tariff> iterator = tariffs.iterator(); isValid && iterator.hasNext(); ) {
            int price = iterator.next().getPrice();
            isValid = price >= Constants.TAX_MIN_PRICE && price <= Constants.TAX_MAX_PRICE;
        }
        return isValid;
    }

    @Override
    public void updateSource() {
        super.updateSource();
        TariffConfig configuration = (TariffConfig) source;
        configuration.tariffs.clear();
        configuration.tariffs.addAll(tariffs);
    }

    @Override
    public void buildNode() {
        super.buildNode();
        ObservableList<Node> children = node.getChildren();
        CarType[] carTypes = CarType.values();
        for (int i = 0; i < carTypes.length; i++) {
            if (i != 0) {
                children.add(new Separator(Orientation.HORIZONTAL));
            }
            children.add(new Label(String.valueOf(carTypes[i])));
            for (Tariff tariff : filterBy(carTypes[i])) {
                children.add(tariff.getPriceSetting().getNode());
            }
        }
    }

    public List<Tariff> getTariffs() {
        return tariffs;
    }

    public List<Tariff> filterBy(TimeType type) {
        return filter(tariffs, type);
    }

    public static List<Tariff> filter(List<Tariff> tariffs, TimeType type) {
        return tariffs.stream().filter(tariff -> tariff.getTimeType() == type).collect(Collectors.toList());
    }

    public List<Tariff> filterBy(CarType type) {
        return filter(tariffs, type);
    }

    public static List<Tariff> filter(List<Tariff> tariffs, CarType type) {
        return tariffs.stream().filter(tariff -> tariff.getCarType() == type).collect(Collectors.toList());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TariffConfig)) return false;
        if (!super.equals(o)) return false;
        TariffConfig that = (TariffConfig) o;
        return tariffs.equals(that.tariffs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), tariffs);
    }

    @Override
    public TariffConfig clone() {
        TariffConfig configuration = (TariffConfig) super.clone();
        configuration.tariffs = new ArrayList<>(tariffs.size());
        for (Tariff tariff : tariffs) {
            configuration.tariffs.add(tariff.clone());
        }
        return configuration;
    }
}