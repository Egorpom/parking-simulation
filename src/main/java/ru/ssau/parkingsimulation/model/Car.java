package ru.ssau.parkingsimulation.model;

import javafx.animation.Animation;
import javafx.beans.binding.DoubleBinding;
import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;
import ru.ssau.parkingsimulation.view.INode;

public class Car implements INode {
    protected CarType type;
    protected Rectangle node;
    protected Animation animation;

    public Car(CarType type) {
        this.type = type;
    }

    public CarType getType() {
        return type;
    }

    public Animation getAnimation() {
        return animation;
    }

    public void setAnimation(Animation animation) {
        this.animation = animation;
    }

    @Override
    public Rectangle getNode() {
        if (node == null) {
            buildNode();
        }
        return node;
    }

    @Override
    public void buildNode() {
        node = new Rectangle();
        node.setFill(new ImagePattern(new Image("/image/car/" + type.name() + ".png")));
        DoubleBinding carSize = Road.INSTANCE.getNode().heightProperty().divide(2);
        node.heightProperty().bind(carSize);
        node.widthProperty().bind(carSize);
    }
}