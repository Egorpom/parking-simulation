package ru.ssau.parkingsimulation.model;

public enum CarType {
    CAR("Легковой автомобиль", "легкового"),
    TRUCK("Грузовой автомобиль", "грузового");

    private String[] titles;

    CarType(String... titles) {
        this.titles = titles;
    }

    public String getTitle() {
        return titles[1];
    }

    @Override
    public String toString() {
        return titles[0];
    }
}