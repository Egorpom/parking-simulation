package ru.ssau.parkingsimulation.model;

import com.google.gson.*;
import ru.ssau.parkingsimulation.config.Configuration;
import ru.ssau.parkingsimulation.config.setting.IntegerSetting;
import ru.ssau.parkingsimulation.util.Constants;

import java.lang.reflect.Type;
import java.util.Objects;

public class ModelConfig extends Configuration {
    private IntegerSetting height = new IntegerSetting("Количество строительных мест по вертикали", Constants.CONSTRUCTION_MIN_IN_COLUMN, true);
    private IntegerSetting width = new IntegerSetting("Количество строительных мест по горизонтали", Constants.CONSTRUCTION_MIN_IN_ROW, true);

    public ModelConfig() {
        super("Модель");
    }

    public int getHeight() {
        return height.getValue();
    }

    public void setHeight(int height) {
        this.height.setValue(height);
    }

    public int getWidth() {
        return width.getValue();
    }

    public void setWidth(int width) {
        this.width.setValue(width);
    }

    @Override
    public boolean isValid() {
        return super.isValid()
                && getWidth() >= Constants.CONSTRUCTION_MIN_IN_ROW
                && getWidth() <= Constants.CONSTRUCTION_MAX_IN_ROW
                && getHeight() >= Constants.CONSTRUCTION_MIN_IN_COLUMN
                && getHeight() <= Constants.CONSTRUCTION_MAX_IN_COLUMN;
    }

    @Override
    public void updateSource() {
        super.updateSource();
        ModelConfig config = (ModelConfig) source;
        config.width.setValue(width.getValue());
        config.height.setValue(height.getValue());
    }

    @Override
    public void buildNode() {
        super.buildNode();
        node.getChildren().addAll(height.getNode(), width.getNode());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ModelConfig)) return false;
        if (!super.equals(o)) return false;
        ModelConfig that = (ModelConfig) o;
        return height.equals(that.height) && width.equals(that.width);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), height, width);
    }

    @Override
    public ModelConfig clone() {
        ModelConfig modelConfig = (ModelConfig) super.clone();
        modelConfig.height = (IntegerSetting) height.clone();
        modelConfig.width = (IntegerSetting) width.clone();
        return modelConfig;
    }

    public static class JsonAdapter implements JsonSerializer<ModelConfig>, JsonDeserializer<ModelConfig> {
        @Override
        public JsonElement serialize(ModelConfig src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("width", src.width.getValue());
            jsonObject.addProperty("height", src.height.getValue());
            return jsonObject;
        }

        @Override
        public ModelConfig deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject jsonObject = json.getAsJsonObject();
            ModelConfig config = new ModelConfig();
            config.setWidth(jsonObject.get("width").getAsInt());
            config.setHeight(jsonObject.get("height").getAsInt());
            return config;
        }
    }
}