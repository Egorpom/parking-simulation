package ru.ssau.parkingsimulation.model;

import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Border;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import ru.ssau.parkingsimulation.model.construction.Construction;
import ru.ssau.parkingsimulation.model.construction.IConstructionType;
import ru.ssau.parkingsimulation.util.Constants;
import ru.ssau.parkingsimulation.view.INode;
import ru.ssau.parkingsimulation.view.pane.ConstructionPane;
import ru.ssau.parkingsimulation.view.pane.SquaredPaneWrapper;

public class ConstructionToolbar implements INode, EventHandler<MouseEvent> {
    public static final ConstructionToolbar INSTANCE = new ConstructionToolbar();
    private boolean editMode;
    protected HBox node;
    protected Construction selectedConstruction = null;

    private ConstructionToolbar() {
    }

    public IConstructionType getSelectedCType() {
        if (selectedConstruction == null) {
            return null;
        }
        return selectedConstruction.getType();
    }

    public boolean isEditMode() {
        return editMode;
    }

    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    @Override
    public HBox getNode() {
        if (node == null) {
            buildNode();
        }
        return node;
    }

    @Override
    public void buildNode() {
        node = new HBox();
        node.setPadding(Constants.INSETS_DEFAULT);
        node.setSpacing(10);

        //Init children
        ObservableList<Node> children = node.getChildren();
        for (IConstructionType type : Constants.ENUM_COLLECTOR_CONSTRUCTIONS.values()) {
            Pane pane = type.createConstruction().getNode();
            pane.addEventFilter(MouseEvent.MOUSE_CLICKED, this);
            SquaredPaneWrapper wrapper = new SquaredPaneWrapper(pane);
            HBox.setHgrow(wrapper, Priority.ALWAYS);
            children.add(wrapper);
        }
    }

    @Override
    public void handle(MouseEvent event) {
        if (!(event.getTarget() instanceof ConstructionPane)) {
            return;
        }
        ConstructionPane pane = (ConstructionPane) event.getTarget();
        MouseButton button = event.getButton();
        if (button == MouseButton.PRIMARY || button == MouseButton.SECONDARY) {
            if (selectedConstruction != null) {
                selectedConstruction.getNode().setBorder(Border.EMPTY);
            }
            if (button == MouseButton.PRIMARY) {
                selectedConstruction = pane.getConstruction();
                selectedConstruction.getNode().setBorder(Constants.BORDER_SELECTED);
            } else {
                selectedConstruction = null;
            }
        }
        event.consume();
    }
}