package ru.ssau.parkingsimulation.model;

import com.google.gson.*;
import javafx.geometry.Pos;
import javafx.scene.layout.*;
import ru.ssau.parkingsimulation.model.construction.*;
import ru.ssau.parkingsimulation.simulation.PathManager;
import ru.ssau.parkingsimulation.util.Constants;
import ru.ssau.parkingsimulation.view.INode;
import ru.ssau.parkingsimulation.view.pane.SquaredGridPaneWrapper;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.*;

public class Model implements INode, Serializable, Cloneable {
    public static Model INSTANCE = null;
    protected ModelConfig config = new ModelConfig();
    protected Map<Position, Construction> constructions = new HashMap<>();
    protected transient int hashCode = Integer.MIN_VALUE;
    protected transient VBox node;
    protected transient GridPane modelPane;
    protected transient Map<IConstructionType, Construction> singleConsCache = new HashMap<>();

    private Model() {
    }

    public static Model init(int width, int height) {
        INSTANCE = new Model();
        INSTANCE.config.setWidth(width);
        INSTANCE.config.setHeight(height);
        for (int i = 0; i < width; ++i) {
            for (int j = 0; j < height; ++j) {
                Construction construction = ConstructionType.NONE.createConstruction();
                construction.setPosition(new Position(i, j));
                INSTANCE.constructions.put(construction.getPosition(), construction);
            }
        }
        return INSTANCE;
    }

    public boolean isValid() {
        int entryCount = 0;
        int exitCount = 0;
        int paymentCount = 0;
        for (Construction value : constructions.values()) {
            IConstructionType type = value.getType();
            if (type == ConstructionType.ENTRY) {
                ++entryCount;
                if (getConstruction(value.getPosition().applyDirection(Direction.DOWN)) != null) {
                    return false;
                }
            } else if (type == ConstructionType.EXIT) {
                ++exitCount;
                if (getConstruction(value.getPosition().applyDirection(Direction.DOWN)) != null) {
                    return false;
                }
            } else if (type == ConstructionType.PAYMENT_BOOTH) {
                ++paymentCount;
            } else if (type == ConstructionType.PARKING_PLACE_CAR || type == ConstructionType.PARKING_PLACE_TRUCK) {
                if (!PathManager.isPathsExists(value, ConstructionType.ENTRY, ConstructionType.EXIT, ConstructionType.PAYMENT_BOOTH)) {
                    return false;
                }
            }
        }
        return entryCount == Constants.CONSTRUCTION_ENTRY_COUNT
                && exitCount == Constants.CONSTRUCTION_EXIT_COUNT
                && paymentCount == Constants.CONSTRUCTION_PAYMENT_BOOTH_COUNT;
    }

    public boolean isChanged() {
        return hashCode != hashCode();
    }

    public void saveState() {
        hashCode = hashCode();
    }

    public ModelConfig getConfig() {
        return config;
    }

    public Map<Position, Construction> getConstructions() {
        return constructions;
    }

    public Construction getConstruction(Position position) {
        return constructions.get(position);
    }

    public void addConstruction(Construction construction, int x, int y) {
        addConstruction(construction, x, y, 0.0D);
    }

    public void addConstruction(Construction construction, int x, int y, double rotation) {
        Position position = new Position(x, y, rotation);
        addConstruction(construction, position);
    }

    public void addConstruction(Construction construction, Position position) {
        //TODO test rotation
        construction.setPosition(position);
        construction.getNode().setRotate(position.getAngle());
        constructions.put(position, construction);
        modelPane.add(construction.getNode(), position.getX(), position.getY());
    }

    public void replaceConstruction(Construction construction, int x, int y) {
        replaceConstruction(construction, x, y, 0.0D);
    }

    public void replaceConstruction(Construction construction, int x, int y, double rotation) {
        replaceConstruction(construction, new Position(x, y, rotation));
    }

    public void replaceConstruction(Construction construction, Position position) {
        Construction oldConstruction = constructions.remove(position);
        modelPane.getChildren().remove(oldConstruction.getNode());
        addConstruction(construction, position);
    }

    public void refreshCache() {
        singleConsCache.clear();
        for (Construction value : constructions.values()) {
            IConstructionType type = value.getType();
            if (type == ConstructionType.ENTRY
                    || type == ConstructionType.EXIT
                    || type == ConstructionType.PAYMENT_BOOTH) {
                singleConsCache.put(type, value);
            }
        }
    }

    public Construction getConstructionFromCache(IConstructionType type) {
        return singleConsCache.get(type);
    }

    public GridPane getModelPane() {
        return modelPane;
    }

    @Override
    public VBox getNode() {
        if (node == null) {
            buildNode();
        }
        return node;
    }

    @Override
    public void buildNode() {
        node = new VBox();
        modelPane = new GridPane();
        modelPane.setAlignment(Pos.CENTER);
        int width = config.getWidth();
        int height = config.getHeight();
        List<ColumnConstraints> columnConstraints = new ArrayList<>(width);
        List<RowConstraints> rowConstraints = new ArrayList<>(height);
        for (int i = 0; i < width; i++) {
            ColumnConstraints columnConstraint = new ColumnConstraints();
            columnConstraint.setPercentWidth(100D / width);
            columnConstraints.add(columnConstraint);
        }
        for (int i = 0; i < height; i++) {
            RowConstraints rowConstraint = new RowConstraints();
            rowConstraint.setPercentHeight(100D / height);
            rowConstraints.add(rowConstraint);
        }
        modelPane.getRowConstraints().addAll(rowConstraints);
        modelPane.getColumnConstraints().addAll(columnConstraints);

        for (Map.Entry<Position, Construction> entry : constructions.entrySet()) {
            modelPane.add(entry.getValue().getNode(), entry.getKey().getX(), entry.getKey().getY());
        }
        SquaredGridPaneWrapper wrapper = new SquaredGridPaneWrapper(modelPane);
        VBox.setVgrow(wrapper, Priority.ALWAYS);
        VBox.setVgrow(Road.INSTANCE.getNode(), Priority.ALWAYS);
        node.getChildren().addAll(wrapper, Road.INSTANCE.getNode());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Model)) return false;
        Model model = (Model) o;
        return config.equals(model.config) && constructions.equals(model.constructions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(config, constructions);
    }

    @Override
    public Model clone() {
        Model model = null;
        try {
            model = (Model) super.clone();
            model.config = config.clone();
            model.constructions = new HashMap<>();
            for (Construction value : constructions.values()) {
                Construction newConstruction = value.clone();
                model.constructions.put(newConstruction.getPosition(), newConstruction);
            }
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return model;
    }

    public static class JsonAdapter implements JsonSerializer<Model>, JsonDeserializer<Model> {
        @Override
        public JsonElement serialize(Model src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.add("config", context.serialize(src.config));
            JsonArray constructions = new JsonArray();
            for (Construction value : src.constructions.values()) {
                constructions.add(context.serialize(value));
            }
            jsonObject.add("constructions", constructions);
            return jsonObject;
        }

        @Override
        public Model deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject jsonObject = json.getAsJsonObject();
            Model model = new Model();
            model.config = context.deserialize(jsonObject.get("config"), ModelConfig.class);
            model.constructions = new HashMap<>();
            JsonArray constructions = jsonObject.get("constructions").getAsJsonArray();
            for (JsonElement element : constructions) {
                Construction construction = context.deserialize(element, Construction.class);
                model.constructions.put(construction.getPosition(), construction);
            }
            return model;
        }
    }
}