package ru.ssau.parkingsimulation.model;

import javafx.scene.image.Image;
import javafx.scene.layout.*;
import ru.ssau.parkingsimulation.model.construction.ConstructionType;
import ru.ssau.parkingsimulation.util.Constants;
import ru.ssau.parkingsimulation.view.INode;

public class Road implements INode {
    public static Road INSTANCE = new Road();
    protected Pane node;

    private Road() {
    }

    public void clearNode() {
        node = null;
    }

    @Override
    public Pane getNode() {
        if (node == null) {
            buildNode();
        }
        return node;
    }

    @Override
    public void buildNode() {
        node = new Pane();
        node.setBackground(new Background(new BackgroundImage(
                new Image("/image/construction/" + ConstructionType.ROAD + ".png"),
                BackgroundRepeat.REPEAT,
                BackgroundRepeat.REPEAT,
                BackgroundPosition.DEFAULT,
                Constants.BACKGROUND_SIZE_DEFAULT
        )));
        Pane modelPane = Model.INSTANCE.getNode();
        int height = Model.INSTANCE.getConfig().getHeight() + 1;
        node.minHeightProperty().bind(modelPane.heightProperty().divide(height));
        node.maxHeightProperty().bind(modelPane.heightProperty().divide(height));
    }
}