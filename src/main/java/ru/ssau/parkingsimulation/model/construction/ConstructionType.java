package ru.ssau.parkingsimulation.model.construction;

public enum ConstructionType implements IConstructionType {
    NONE,
    PARKING_PLACE_CAR,
    PARKING_PLACE_TRUCK,
    LAWN,
    ENTRY,
    EXIT,
    ROAD,
    PAYMENT_BOOTH;

    public Construction createConstruction() {
        return new Construction(this);
    }

    @Override
    public int extOrdinal() {
        return ordinal();
    }
}