package ru.ssau.parkingsimulation.model.construction;

import com.google.gson.*;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.event.EventHandler;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.util.Duration;
import ru.ssau.parkingsimulation.model.Car;
import ru.ssau.parkingsimulation.model.ConstructionToolbar;
import ru.ssau.parkingsimulation.model.Model;
import ru.ssau.parkingsimulation.simulation.Simulation;
import ru.ssau.parkingsimulation.simulation.SimulationToolbar;
import ru.ssau.parkingsimulation.util.Constants;
import ru.ssau.parkingsimulation.view.INode;
import ru.ssau.parkingsimulation.view.ViewUtils;
import ru.ssau.parkingsimulation.view.pane.ConstructionPane;
import ru.ssau.parkingsimulation.view.pane.StatisticPane;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Objects;

public class Construction implements INode, EventHandler<MouseEvent>, Serializable, Cloneable {
    protected transient ConstructionPane node;
    protected transient boolean busy;
    protected IConstructionType type;
    protected Position position;
    protected transient long inTime = 0;
    protected transient SimpleLongProperty parkingTime = new SimpleLongProperty();
    protected transient SimpleLongProperty outTime = new SimpleLongProperty();
    protected transient SimpleIntegerProperty payment = new SimpleIntegerProperty();

    public Construction(IConstructionType type) {
        this.type = type;
    }

    public IConstructionType getType() {
        return type;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public boolean isBusy() {
        return busy;
    }

    public void setBusy(boolean busy) {
        if (busy && this.busy) {
            inTime = SimulationToolbar.INSTANCE.getModelTime();
            outTime.bind(parkingTime.add(inTime));
            StatisticPane.addConstruction(this);
        } else if (!busy) {
            StatisticPane.removeConstruction(this);
            outTime.unbind();
            payment.unbind();
            parkingTime.unbind();
        }
        this.busy = busy;
    }

    public void setParkingTime(Car car, Duration duration) {
        parkingTime.bind(
                Bindings.createLongBinding(() -> Double.valueOf(duration.toSeconds() / car.getAnimation().getRate() * SimulationToolbar.INSTANCE.getSpeedRateObservable().getValue()).longValue(),
                        car.getAnimation().rateProperty()
                ));
        payment.bind(Bindings.createIntegerBinding(
                () -> Simulation.INSTANCE.getConfig().getTariffConfig().getActiveTariff(car).getPrice() * parkingTime.getValue().intValue(),
                parkingTime,
                SimulationToolbar.INSTANCE.getTimeTypeObservable()
        ));
    }

    public String getInTime() {
        return ViewUtils.formatModelTime(inTime);
    }

    public SimpleLongProperty getParkingTime() {
        return parkingTime;
    }

    public SimpleLongProperty getOutTime() {
        return outTime;
    }

    public SimpleIntegerProperty getPayment() {
        return payment;
    }

    @Override
    public ConstructionPane getNode() {
        if (node == null) {
            buildNode();
        }
        return node;
    }

    @Override
    public void buildNode() {
        node = new ConstructionPane(this);
        Tooltip.install(node, new Tooltip(String.valueOf(type)));
        node.addEventHandler(MouseEvent.MOUSE_CLICKED, this);
        node.setBackground(new Background(new BackgroundImage(
                new Image("/image/construction/" + type + ".png"),
                BackgroundRepeat.NO_REPEAT,
                BackgroundRepeat.NO_REPEAT,
                BackgroundPosition.CENTER,
                Constants.BACKGROUND_SIZE_DEFAULT
        )));
    }

    @Override
    public void handle(MouseEvent event) {
        if (!ConstructionToolbar.INSTANCE.isEditMode()) {
            return;
        }
        IConstructionType cType = null;
        MouseButton button = event.getButton();
        IConstructionType selectedCType = ConstructionToolbar.INSTANCE.getSelectedCType();
        if (button == MouseButton.PRIMARY && selectedCType != null && selectedCType != type) {
            cType = selectedCType;
        } else if (button == MouseButton.SECONDARY && selectedCType != ConstructionType.NONE) {
            cType = ConstructionType.NONE;
        }
        if (cType != null) {
            Model.INSTANCE.replaceConstruction(cType.createConstruction(), position);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Construction)) return false;
        Construction that = (Construction) o;
        return type == that.type && Objects.equals(position, that.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, position);
    }

    @Override
    public Construction clone() {
        Construction construction = null;
        try {
            construction = (Construction) super.clone();
            construction.position = position.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return construction;
    }

    @Override
    public String toString() {
        return GridPane.getColumnIndex(node) + ":" + GridPane.getRowIndex(node);
    }

    public static class JsonAdapter implements JsonDeserializer<Construction> {
        @Override
        public Construction deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject jsonObject = json.getAsJsonObject();
            IConstructionType type = Constants.ENUM_COLLECTOR_CONSTRUCTIONS.valueOf(jsonObject.get("type").getAsString());
            Construction construction = type.createConstruction();
            construction.position = context.deserialize(jsonObject.get("position"), Position.class);
            return construction;
        }
    }
}