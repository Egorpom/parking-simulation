package ru.ssau.parkingsimulation.model.construction;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class Position implements Serializable, Cloneable {

    private int x;
    private int y;
    private double angle;

    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Position(int x, int y, double angle) {
        this(x, y);
        this.angle = angle;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public Position applyDirection(Direction direction) {
        return new Position(x + direction.getDX(), y + direction.getDY());
    }

    public Position applyDirections(List<Direction> directions) {
        int dx = 0;
        int dy = 0;
        for (Direction direction : directions) {
            dx += direction.getDX();
            dy += direction.getDY();
        }
        return new Position(x + dx, y + dy);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Position)) return false;
        Position position = (Position) o;
        return x == position.x && y == position.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public Position clone() {
        Position position = null;
        try {
            position = (Position) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return position;
    }
}