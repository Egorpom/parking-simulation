package ru.ssau.parkingsimulation.model.construction;

import ru.ssau.parkingsimulation.util.IExtendableEnum;

public interface IConstructionType extends IExtendableEnum {
    Construction createConstruction();
}