package ru.ssau.parkingsimulation.model.construction;

public enum Direction {
    LEFT(-1, 0),
    UP(0, -1),
    RIGHT(1, 0),
    DOWN(0, 1);

    private final int dx;
    private final int dy;

    Direction(int dx, int dy) {
        this.dx = dx;
        this.dy = dy;
    }

    public int getDX() {
        return dx;
    }

    public int getDY() {
        return dy;
    }
}