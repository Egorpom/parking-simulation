package ru.ssau.parkingsimulation;

import javafx.application.Application;
import javafx.stage.Stage;
import ru.ssau.parkingsimulation.model.construction.ConstructionType;
import ru.ssau.parkingsimulation.util.Constants;
import ru.ssau.parkingsimulation.view.ViewUtils;

public class Main extends Application {

    @Override
    public void start(Stage stage) {
        ViewUtils.openWindow(null, stage, Constants.PANE_MAIN_URL, Constants.PANE_MAIN_TITLE);
    }

    public static void main(String[] args) {
        Constants.ENUM_COLLECTOR_CONSTRUCTIONS.collect(ConstructionType.values());
        launch(args);
    }
}