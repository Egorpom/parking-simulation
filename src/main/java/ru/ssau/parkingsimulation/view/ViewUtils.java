package ru.ssau.parkingsimulation.view;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import ru.ssau.parkingsimulation.Main;
import ru.ssau.parkingsimulation.util.Constants;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public final class ViewUtils {

    public static void openNewModalWindow(Window owner, String resourceURL, String title) {
        openWindow(owner, null, Main.class.getResource(resourceURL), title);
    }

    public static void openNewModalWindow(Window owner, URL resourceURL, String title) {
        openWindow(owner, null, resourceURL, title);
    }

    public static void openWindow(Window owner, Stage stage, String resourceURL, String title) {
        openWindow(owner, stage, Main.class.getResource(resourceURL), title);
    }

    public static void openWindow(Window owner, Stage stage, URL resourceURL, String title) {
        openWindow(owner, stage, loadPaneFromFXML(resourceURL), title);
    }

    public static void openWindow(Window owner, Stage stage, Parent parent, String title) {
        if (stage == null) {
            stage = new Stage();
        }
        stage.setTitle(title);
        stage.setScene(new Scene(parent));
        bindMinSizeToWindow(parent);
        if (owner != null) {
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.initOwner(owner);
            stage.showAndWait();
        } else {
            stage.show();
        }
    }

    public static Parent loadPaneFromFXML(URL resourceURL) {
        Parent parent = null;
        try {
            parent = FXMLLoader.load(resourceURL);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return parent;
    }

    public static void bindMinSizeToWindow(Parent parent) {
        if (!(parent instanceof Region)) {
            return;
        }
        Window window = parent.getScene().getWindow();
        if (!(window instanceof Stage)) {
            return;
        }
        bindMinSizeToWindow((Stage) window, (Region) parent);
    }

    public static void bindMinSizeToWindow(Stage stage, Region region) {
        stage.minHeightProperty().bind(region.minHeightProperty());
        stage.minWidthProperty().bind(region.minWidthProperty());
    }

    public static Optional<String> showInputAndGetOptional(String title, String headerText, String contextText, String defaultValue) {
        TextInputDialog inputDialog = new TextInputDialog(defaultValue);
        inputDialog.setTitle(title);
        inputDialog.setHeaderText(headerText);
        inputDialog.setContentText(contextText);
        return inputDialog.showAndWait();
    }

    public static void replacePaneOnSplitPane(SplitPane splitPane, Node node, int order) {
        double[] dividerPositions = splitPane.getDividerPositions();
        ObservableList<Node> items = splitPane.getItems();
        if (order >= 0 && order < items.size()) {
            items.remove(order);
        }
        addPaneOnSplitPane(splitPane, node, order, dividerPositions);
    }

    public static void addPaneOnSplitPane(SplitPane splitPane, Node node, int order, double... dividerPositions) {
        SplitPane.setResizableWithParent(node, false);
        splitPane.getItems().add(order, node);
        splitPane.setDividerPositions(dividerPositions);
    }

    public static void setAnchors(Node child, double... anchors) {
        if (anchors.length == 1) {
            setAnchors(child, anchors[0], anchors[0], anchors[0], anchors[0]);
        } else if (anchors.length == 2) {
            setAnchors(child, anchors[0], anchors[0], anchors[1], anchors[1]);
        } else if (anchors.length == 4) {
            setAnchors(child, anchors[0], anchors[1], anchors[2], anchors[3]);
        }
    }

    public static void setAnchors(Node child, double top, double bottom, double left, double right) {
        AnchorPane.setTopAnchor(child, top);
        AnchorPane.setBottomAnchor(child, bottom);
        AnchorPane.setLeftAnchor(child, left);
        AnchorPane.setRightAnchor(child, right);
    }

    public static Background getColoredBackground(Color color) {
        return getColoredBackground(color, CornerRadii.EMPTY, Insets.EMPTY);
    }

    public static Background getColoredBackground(Color color, CornerRadii cornerRadii, Insets insets) {
        return new Background(new BackgroundFill(color, cornerRadii, insets));
    }

    public static Optional<ButtonType> showConfirmation(String contentText) {
        return showAlert(Alert.AlertType.CONFIRMATION, contentText);
    }

    public static Optional<ButtonType> showError(String contentText) {
        return showAlert(Alert.AlertType.ERROR, contentText);
    }

    public static Optional<ButtonType> showAlert(Alert.AlertType alertType, String contentText) {
        Alert alert = new Alert(alertType, contentText);
        return alert.showAndWait();
    }

    public static void bindSizeProperties(Region source, Region child, double min, double max, boolean isWidth) {
        if (isWidth) {
            bindSizeProperties(source.widthProperty(), child.minWidthProperty(), child.maxWidthProperty(), min, max);
        } else {
            bindSizeProperties(source.heightProperty(), child.minHeightProperty(), child.maxHeightProperty(), min, max);
        }
    }

    public static void bindSizeProperties(ReadOnlyDoubleProperty sourceProp, DoubleProperty minProp, DoubleProperty maxProp, double min, double max) {
        minProp.bind(sourceProp.multiply(min));
        maxProp.bind(sourceProp.multiply(max));
    }

    public static String formatRealTime(long seconds) {
        return Constants.FORMATTER_LONG.format(TimeUnit.SECONDS.toMinutes(seconds)) + ":" + Constants.FORMATTER_LONG.format(seconds % 60);
    }

    public static String formatModelTime(long seconds) {
        seconds = (long) (seconds * Constants.TIME_RATE_SEC_TO_MIN);
        return Constants.FORMATTER_LONG.format(TimeUnit.MINUTES.toDays(seconds)) + ":" + Constants.FORMATTER_LONG.format(TimeUnit.MINUTES.toHours(seconds) % 24) + ":" + Constants.FORMATTER_LONG.format(seconds % 60);
    }
}