package ru.ssau.parkingsimulation.view.controller;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Orientation;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import ru.ssau.parkingsimulation.model.ConstructionToolbar;
import ru.ssau.parkingsimulation.model.Model;
import ru.ssau.parkingsimulation.model.Road;
import ru.ssau.parkingsimulation.simulation.Simulation;
import ru.ssau.parkingsimulation.simulation.SimulationState;
import ru.ssau.parkingsimulation.simulation.SimulationToolbar;
import ru.ssau.parkingsimulation.util.Constants;
import ru.ssau.parkingsimulation.view.ViewUtils;
import ru.ssau.parkingsimulation.view.pane.SizeInputDialog;
import ru.ssau.parkingsimulation.view.pane.StatisticPane;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.net.URISyntaxException;
import java.util.Optional;

public class MainController {
    public static AnchorPane mainPane;
    @FXML
    private AnchorPane rootPane;
    @FXML
    private SplitPane splitPane;
    //Menu
    @FXML
    private MenuItem menuSave;
    @FXML
    private MenuItem menuSaveAs;
    @FXML
    private MenuItem menuClose;
    @FXML
    private CheckMenuItem checkEditMode;
    @FXML
    private CheckMenuItem checkSimMode;

    private FileChooser fileChooser = new FileChooser();
    private File lastUsedFile = null;

    public void initialize() {
        rootPane.sceneProperty().addListener((observable, oldScene, newScene) -> newScene.windowProperty().addListener((observable1, oldWindow, newWindow) -> newWindow.setOnCloseRequest(event -> exit())));
        mainPane = rootPane;
        Simulation simulation = Simulation.INSTANCE;
        //Model menu
        BooleanBinding isModelNotExists = Bindings.createBooleanBinding(() -> Model.INSTANCE == null, splitPane.getItems());
        BooleanBinding isSimStopped = Bindings.createBooleanBinding(() -> simulation.getState() == SimulationState.STOPPED, simulation.stateProperty());
        menuSave.disableProperty().bind(isModelNotExists);
        menuSaveAs.disableProperty().bind(isModelNotExists);
        menuClose.disableProperty().bind(isModelNotExists);
        checkEditMode.disableProperty().bind(isModelNotExists.or(isSimStopped.not()));
        //Simulation menu
        checkSimMode.disableProperty().bind(checkEditMode.disableProperty());
        //Construction Toolbar init
        ConstructionToolbar editToolbar = ConstructionToolbar.INSTANCE;
        ViewUtils.bindSizeProperties(splitPane, editToolbar.getNode(), 0.1, 0.3, false);
        editToolbar.setEditMode(checkEditMode.isSelected());
        checkEditMode.selectedProperty().addListener((observable, oldValue, newValue) -> {
            editToolbar.setEditMode(newValue);
            if (!checkEditMode.isDisable() && newValue) {
                if (checkSimMode.isSelected()) {
                    checkSimMode.setSelected(false);
                }
                ViewUtils.addPaneOnSplitPane(splitPane, editToolbar.getNode(), 1, 0.8);
            } else {
                splitPane.getItems().remove(1);
            }
        });
        //Simulation Toolbar init
        SimulationToolbar simToolbar = SimulationToolbar.INSTANCE;
        ViewUtils.bindSizeProperties(splitPane, simToolbar.getNode(), 0.1, 0.3, true);
        checkSimMode.selectedProperty().addListener((observable, oldValue, newValue) -> {
            simToolbar.setSimMode(newValue);
            if (!checkSimMode.isDisable() && newValue) {
                if (checkEditMode.isSelected()) {
                    checkEditMode.setSelected(false);
                }
                splitPane.setOrientation(Orientation.HORIZONTAL);
                ViewUtils.addPaneOnSplitPane(splitPane, simToolbar.getNode(), 0, 0.23);
                ViewUtils.openWindow(null, null, StatisticPane.INSTANCE = new StatisticPane(), "Статистика");
                StatisticPane.INSTANCE.getScene().getWindow().setOnCloseRequest(event -> StatisticPane.INSTANCE = null);
            } else {
                splitPane.setOrientation(Orientation.VERTICAL);
                splitPane.getItems().remove(0);
                if (StatisticPane.INSTANCE != null) {
                    ((Stage) StatisticPane.INSTANCE.getScene().getWindow()).close();
                    StatisticPane.INSTANCE = null;
                }
            }
        });

        BooleanBinding isSimPaused = Bindings.createBooleanBinding(() -> simulation.getState() == SimulationState.PAUSED, simulation.stateProperty());
        Button[] buttons = simToolbar.getButtons();
        buttons[0].disableProperty().bind(isModelNotExists.or(isSimStopped.or(isSimPaused).not()));
        buttons[0].setOnAction(this::startSim);
        buttons[1].disableProperty().bind(isModelNotExists.or(isSimStopped).or(isSimPaused));
        buttons[1].setOnAction(event -> simulation.pause());
        buttons[2].disableProperty().bind(isModelNotExists.or(isSimStopped));
        buttons[2].setOnAction(event -> simulation.restart());
        buttons[3].disableProperty().bind(isModelNotExists.or(isSimStopped));
        buttons[3].setOnAction(event -> Simulation.INSTANCE.stop());

        fileChooser.setInitialDirectory(Constants.DIR_DEFAULT);
        fileChooser.getExtensionFilters().addAll(Constants.EXTENSION_FILTERS_DEFAULT);
    }

    @FXML
    private void openSettingWindow() {
        ViewUtils.openNewModalWindow(rootPane.getScene().getWindow(), Constants.PANE_SETTINGS_URL, Constants.PANE_SETTINGS_TITLE);
    }

    @FXML
    private void createNewModel() {
        SizeInputDialog dialog = new SizeInputDialog();
        Optional<Integer[]> optional = dialog.showAndWait();
        if (!optional.isPresent()) {
            return;
        }
        Integer[] integers = optional.get();
        int width = integers[0];
        int height = integers[1];
        if (!closeModel()) {
            return;
        }
        Model.init(width, height);
        addModelToPane();
    }

    @FXML
    private void openModel() {
        fileChooser.setTitle("Выберите файл модели");
        File modelFile = fileChooser.showOpenDialog(rootPane.getScene().getWindow());
        if (modelFile == null) {
            return;
        }
        try (FileReader reader = new FileReader(modelFile)) {
            if (!closeModel()) {
                return;
            }
            Model.INSTANCE = Constants.GSON.fromJson(reader, Model.class);
            Model.INSTANCE.saveState();
            addModelToPane();
            lastUsedFile = modelFile;
        } catch (Exception e) {
            e.printStackTrace();
            ViewUtils.showError("Ошибка при загрузке модели");
        }
    }

    @FXML
    private void saveModel(ActionEvent event) {
        File modelFile = lastUsedFile;
        if (modelFile == null || event.getSource() == menuSaveAs) {
            fileChooser.setTitle("Введите имя файла модели");
            modelFile = fileChooser.showSaveDialog(rootPane.getScene().getWindow());
        }
        if (modelFile == null) {
            return;
        }
        try (FileWriter writer = new FileWriter(modelFile)) {
            writer.write(Constants.GSON.toJson(Model.INSTANCE));
            Model.INSTANCE.saveState();
            lastUsedFile = modelFile;
        } catch (Exception e) {
            e.printStackTrace();
            ViewUtils.showError("Ошибка при сохранении модели");
        }
    }

    @FXML
    private void exit() {
        if (closeModel()) {
            System.exit(0);
        }
    }

    @FXML
    private boolean closeModel() {
        if (!canCloseModel()) {
            return false;
        }
        if (Simulation.INSTANCE.getState() != SimulationState.STOPPED) {
            Simulation.INSTANCE.stop();
        }
        Model.INSTANCE = null;
        Road.INSTANCE.clearNode();
        splitPane.getItems().clear();
        lastUsedFile = null;
        return true;
    }

    @FXML
    private void openUserGuide() {
        try {
            WebView webView = new WebView();
            webView.getEngine().load(getClass().getResource("/html/Glavnaya.html").toURI().toString());
            ViewUtils.openWindow(rootPane.getScene().getWindow(), null, webView, "Руководство пользователя");
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private void startSim(ActionEvent event) {
        if (!Model.INSTANCE.isValid()) {
            ViewUtils.showError("Неверно составлена модель.");
            return;
        }
        Simulation.INSTANCE.start();
    }

    private boolean canCloseModel() {
        if (Model.INSTANCE == null || !Model.INSTANCE.isChanged()) {
            return true;
        }
        Optional<ButtonType> optional = ViewUtils.showConfirmation("Модель была изменена. Все несохраненные изменения будут потеряны. Продолжить?");
        return optional.isPresent() && optional.get() == ButtonType.OK;
    }

    private void addModelToPane() {
        ViewUtils.addPaneOnSplitPane(splitPane, Model.INSTANCE.getNode(), 0);
        if (ConstructionToolbar.INSTANCE.isEditMode()) {
            ViewUtils.addPaneOnSplitPane(splitPane, ConstructionToolbar.INSTANCE.getNode(), 1, 0.8);
        } else if (SimulationToolbar.INSTANCE.isSimMode()) {
            ViewUtils.addPaneOnSplitPane(splitPane, SimulationToolbar.INSTANCE.getNode(), 0, 0.2);
        }
    }
}