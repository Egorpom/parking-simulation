package ru.ssau.parkingsimulation.view.controller;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ButtonType;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import ru.ssau.parkingsimulation.config.Configuration;
import ru.ssau.parkingsimulation.model.Model;
import ru.ssau.parkingsimulation.simulation.Simulation;
import ru.ssau.parkingsimulation.util.Constants;
import ru.ssau.parkingsimulation.view.ViewUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SettingsController {
    @FXML
    private AnchorPane rootPane;
    @FXML
    private SplitPane splitPane;
    @FXML
    private TreeView<Configuration> treeView;

    public void initialize() {
        //TreeView width
        ViewUtils.bindSizeProperties(splitPane, treeView, 0.1, 0.3, true);
        ViewUtils.addPaneOnSplitPane(splitPane, Constants.PANE_DEFAULT_EMPTY, 1, 0.2);
        TreeItem<Configuration> root = new Configuration().getTreeItem();
        treeView.setRoot(root);
        if (Model.INSTANCE != null) {
            root.getChildren().add(Model.INSTANCE.getConfig().clone().getTreeItem());
        }
        root.getChildren().add(Simulation.INSTANCE.getConfig().clone().getTreeItem());

        treeView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                ViewUtils.replacePaneOnSplitPane(splitPane, newValue.getValue().getNode(), 1);
            }
        });
    }

    @FXML
    private void onSave() {
        for (Configuration configuration : getChangedConfiguration()) {
            if (configuration.isValid()) {
                configuration.updateSource();
            } else {
                Optional<ButtonType> optional = ViewUtils.showConfirmation("Данные были изменены некорректно. Некорректные данные не будут сохранены. Продолжить?");
                if (!optional.isPresent() || optional.get() != ButtonType.OK) {
                    return;
                }
            }
        }
        ((Stage) rootPane.getScene().getWindow()).close();
    }

    @FXML
    private void onCancel() {
        if (getChangedConfiguration().size() > 0) {
            Optional<ButtonType> optional = ViewUtils.showConfirmation("Данные были изменены. Все несохраненные изменения будут потеряны. Продолжить?");
            if (!optional.isPresent() || optional.get() != ButtonType.OK) {
                return;
            }
        }
        ((Stage) rootPane.getScene().getWindow()).close();
    }

    private List<Configuration> getChangedConfiguration() {
        List<Configuration> changedConfiguration = new ArrayList<>();
        ObservableList<TreeItem<Configuration>> children = treeView.getRoot().getChildren();
        for (TreeItem<Configuration> child : children) {
            Configuration configuration = child.getValue();
            if (!configuration.equals(configuration.getSource())) {
                changedConfiguration.add(configuration);
            }
        }
        return changedConfiguration;
    }
}