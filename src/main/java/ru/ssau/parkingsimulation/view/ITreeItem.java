package ru.ssau.parkingsimulation.view;

import javafx.scene.control.TreeItem;

public interface ITreeItem<T> {
    TreeItem<T> getTreeItem();

    void buildTreeItem();
}