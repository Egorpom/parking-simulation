package ru.ssau.parkingsimulation.view.pane;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import ru.ssau.parkingsimulation.view.ViewUtils;

public abstract class FixedRatioPaneWrapper<T extends Node> extends AnchorPane {
    protected T childPane;

    public FixedRatioPaneWrapper(T childPane) {
        super(childPane);
        this.childPane = childPane;
        this.widthProperty().addListener(new SizeChangeListener(true));
        this.heightProperty().addListener(new SizeChangeListener(false));
    }

    public T getChildPane() {
        return childPane;
    }

    protected abstract double[] getChildSize(double newHeight, double newWidth);

    private class SizeChangeListener implements ChangeListener<Number> {
        private boolean isWidth;

        public SizeChangeListener(boolean isWidth) {
            this.isWidth = isWidth;
        }

        @Override
        public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
            double height, width;
            if (isWidth) {
                width = newValue.doubleValue();
                height = getHeight();
            } else {
                width = getWidth();
                height = newValue.doubleValue();
            }
            height = floorEvent(height);
            width = floorEvent(width);
            double[] newAnchors = getChildSize(height, width);
            newAnchors[0] = (height - newAnchors[0]) / 2;
            newAnchors[1] = (width - newAnchors[1]) / 2;
            ViewUtils.setAnchors(childPane, newAnchors[0] * 2, 0, newAnchors[1], newAnchors[1]);
        }
    }

    private static double floorEvent(double d) {
        return Math.floor(d / 2) * 2;
    }
}