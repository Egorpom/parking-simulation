package ru.ssau.parkingsimulation.view.pane;

import javafx.beans.binding.Bindings;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import ru.ssau.parkingsimulation.model.construction.Construction;
import ru.ssau.parkingsimulation.util.Constants;
import ru.ssau.parkingsimulation.view.ViewUtils;

public class StatisticPane extends VBox {
    public static StatisticPane INSTANCE;
    private TableView<Construction> constructionTableView = new TableView<>();

    public StatisticPane() {
        setPrefSize(640, 720);
        setMinSize(320, 360);
        constructionTableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        setVgrow(constructionTableView, Priority.ALWAYS);
        TableColumn<Construction, String> idColumn = new TableColumn<>("ID");
        idColumn.setCellValueFactory(param -> new ReadOnlyStringWrapper(param.getValue().toString()));
        TableColumn<Construction, String> inTimeColumn = new TableColumn<>("Время заезда");
        inTimeColumn.setCellValueFactory(new PropertyValueFactory<>("inTime"));
        TableColumn<Construction, String> parkingTimeColumn = new TableColumn<>("Время стоянки");
        parkingTimeColumn.setCellValueFactory(param -> Bindings.createStringBinding(
                () -> ViewUtils.formatRealTime((long) (param.getValue().getParkingTime().getValue() * Constants.TIME_RATE_SEC_TO_MIN)),
                param.getValue().getParkingTime()
        ));
        TableColumn<Construction, String> outTimeColumn = new TableColumn<>("Время выезда");
        outTimeColumn.setCellValueFactory(param -> Bindings.createStringBinding(
                () -> ViewUtils.formatModelTime(param.getValue().getOutTime().getValue()),
                param.getValue().getOutTime()
        ));
        TableColumn<Construction, String> paymentColumn = new TableColumn<>("Оплата");
        paymentColumn.setCellValueFactory(param -> Bindings.createStringBinding(
                () -> param.getValue().getPayment().getValue().toString(),
                param.getValue().getPayment()
        ));
        constructionTableView.getColumns().addAll(idColumn, inTimeColumn, parkingTimeColumn, outTimeColumn, paymentColumn);
        getChildren().add(constructionTableView);
    }

    public static void addConstruction(Construction construction) {
        if (INSTANCE != null) {
            INSTANCE.constructionTableView.getItems().add(construction);
        }
    }

    public static void removeConstruction(Construction construction) {
        if (INSTANCE != null) {
            INSTANCE.constructionTableView.getItems().remove(construction);
        }
    }
}