package ru.ssau.parkingsimulation.view.pane;

import javafx.scene.layout.Pane;

public class SquaredPaneWrapper extends FixedRatioPaneWrapper<Pane> {
    public SquaredPaneWrapper(Pane children) {
        super(children);
    }

    @Override
    protected double[] getChildSize(double newHeight, double newWidth) {
        double[] newChildSize = new double[2];
        double minSideSize = Double.min(newHeight, newWidth);
        newChildSize[0] = minSideSize;
        newChildSize[1] = minSideSize;
        return newChildSize;
    }
}