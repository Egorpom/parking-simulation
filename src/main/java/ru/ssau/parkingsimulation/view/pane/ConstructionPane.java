package ru.ssau.parkingsimulation.view.pane;

import javafx.scene.layout.Pane;
import ru.ssau.parkingsimulation.model.construction.Construction;

public class ConstructionPane extends Pane {
    private Construction construction;

    public ConstructionPane(Construction construction) {
        this.construction = construction;
    }

    public Construction getConstruction() {
        return construction;
    }
}