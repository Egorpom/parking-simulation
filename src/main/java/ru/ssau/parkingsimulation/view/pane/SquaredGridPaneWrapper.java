package ru.ssau.parkingsimulation.view.pane;

import javafx.scene.layout.GridPane;

public class SquaredGridPaneWrapper extends FixedRatioPaneWrapper<GridPane> {
    public SquaredGridPaneWrapper(GridPane children) {
        super(children);
    }

    @Override
    protected double[] getChildSize(double newHeight, double newWidth) {
        double[] newChildSize = new double[2];
        int columns = childPane.getColumnConstraints().size();
        int rows = childPane.getRowConstraints().size();
        double minSideSize = Double.min(newWidth / columns, newHeight / rows);
        newChildSize[0] = minSideSize * rows;
        newChildSize[1] = minSideSize * columns;
        return newChildSize;
    }
}