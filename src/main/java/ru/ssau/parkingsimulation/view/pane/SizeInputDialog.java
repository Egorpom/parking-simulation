package ru.ssau.parkingsimulation.view.pane;

import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import ru.ssau.parkingsimulation.util.Constants;

public class SizeInputDialog extends Dialog<Integer[]> {

    public SizeInputDialog() {
        this(Constants.CONSTRUCTION_MIN_IN_ROW, Constants.CONSTRUCTION_MIN_IN_COLUMN);
    }

    public SizeInputDialog(Integer width, Integer height) {
        //Labels
        Label heightLabel = new Label("Количество строительных мест по вертикали:");
        Label widthLabel = new Label("Количество строительных мест по горизонтали:");

        //TextFields
        Spinner<Integer> heightSpinner = new Spinner<>();
        heightSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(
                Constants.CONSTRUCTION_MIN_IN_COLUMN,
                Constants.CONSTRUCTION_MAX_IN_COLUMN,
                height,
                1
        ));
        Spinner<Integer> widthSpinner = new Spinner<>();
        widthSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(
                Constants.CONSTRUCTION_MIN_IN_ROW,
                Constants.CONSTRUCTION_MAX_IN_ROW,
                width,
                1
        ));

        //Grid
        GridPane grid = new GridPane();
        grid.setHgap(10D);
        grid.setVgap(10D);
        grid.add(heightLabel, 0, 0);
        grid.add(heightSpinner, 1, 0);
        grid.add(widthLabel, 0, 1);
        grid.add(widthSpinner, 1, 1);

        //DialogPane
        DialogPane dialogPane = getDialogPane();
        setTitle("Создать новую модель");
        dialogPane.setContent(grid);
        dialogPane.getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

        setResultConverter((dialogButton) -> {
            ButtonBar.ButtonData data = dialogButton == null ? null : dialogButton.getButtonData();
            return data == ButtonBar.ButtonData.OK_DONE ? new Integer[]{widthSpinner.getValue(), heightSpinner.getValue()} : null;
        });
    }
}