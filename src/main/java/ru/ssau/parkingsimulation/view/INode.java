package ru.ssau.parkingsimulation.view;

import javafx.scene.Node;

public interface INode {
    Node getNode();

    void buildNode();
}