package ru.ssau.parkingsimulation.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class EnumCollector<T extends IExtendableEnum> {
    private Map<String, T> enums = new HashMap<>();

    public void collect(Class<? extends T> enumClass) {
        collect(enumClass.getEnumConstants());
    }

    public void collect(T[] newEnums) {
        if (newEnums == null) {
            throw new NullPointerException("Enums array can't be null");
        }
        assertExtOrdinalsUnique(newEnums);
        for (T newEnum : newEnums) {
            enums.put(newEnum.name(), newEnum);
        }
    }

    public T valueOf(String name) {
        return enums.get(name);
    }

    public Collection<T> values() {
        return enums.values();
    }

    private void assertExtOrdinalsUnique(T[] newEnums) {
        for (T newEnum : newEnums) {
            for (T value : enums.values()) {
                if (newEnum.extOrdinal() == value.extOrdinal()) {
                    throw new IllegalStateException("ExtOrdinal must be unique");
                }
            }
        }
    }
}