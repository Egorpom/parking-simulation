package ru.ssau.parkingsimulation.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javafx.geometry.Insets;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.util.Duration;
import ru.ssau.parkingsimulation.Main;
import ru.ssau.parkingsimulation.model.Model;
import ru.ssau.parkingsimulation.model.ModelConfig;
import ru.ssau.parkingsimulation.model.construction.Construction;
import ru.ssau.parkingsimulation.model.construction.IConstructionType;
import ru.ssau.parkingsimulation.simulation.TimeType;
import ru.ssau.parkingsimulation.simulation.tariff.Tariff;

import java.io.File;
import java.net.URL;
import java.text.DecimalFormat;

public final class Constants {
    //Panes
    public final static Pane PANE_DEFAULT_EMPTY = new Pane();
    public final static URL PANE_MAIN_URL = Main.class.getResource("/fxml/Main.fxml");
    public final static String PANE_MAIN_TITLE = "Симулятор парковки";
    public final static URL PANE_SETTINGS_URL = Main.class.getResource("/fxml/Settings.fxml");
    public final static String PANE_SETTINGS_TITLE = "Настройки";
    //Insets
    public final static Insets INSETS_DEFAULT = new Insets(10);
    //Images
    public static final BackgroundSize BACKGROUND_SIZE_DEFAULT = new BackgroundSize(
            BackgroundSize.AUTO,
            BackgroundSize.AUTO,
            false,
            false,
            false,
            true
    );
    //Borders
    public static final Border BORDER_SELECTED = new Border(new BorderStroke(Color.BLUE, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderStroke.MEDIUM));
    //Constrains
    public static final int CONSTRUCTION_MIN_IN_ROW = 5;
    public static final int CONSTRUCTION_MIN_IN_COLUMN = 5;
    public static final int CONSTRUCTION_MAX_IN_ROW = 15;
    public static final int CONSTRUCTION_MAX_IN_COLUMN = 15;
    public static final int CONSTRUCTION_ENTRY_COUNT = 1;
    public static final int CONSTRUCTION_EXIT_COUNT = 1;
    public static final int CONSTRUCTION_PAYMENT_BOOTH_COUNT = 1;
    //Cars behavior
    public static final double TIME_MIN_BETWEEN_CAR_SPAWN = 1.0D;
    public static final double TIME_MAX_BETWEEN_CAR_SPAWN = 15.0D;
    public static final Duration DURATION_CAR_SEC = Duration.seconds(2.5);
    public static final double SPEED_RATE_MIN = 0.25D;
    public static final double SPEED_RATE_MAX = 4.0D;
    public static final double TIME_MIN_CAR_PARKING = 5.0D;
    public static final double TIME_MAX_CAR_PARKING = 120.0D;
    public static final int TAX_MIN_PRICE = 50;
    public static final int TAX_MAX_PRICE = 500;
    //Files
    public static final Gson GSON = new GsonBuilder()
            .setPrettyPrinting()
            .enableComplexMapKeySerialization()
            .serializeNulls()
            .registerTypeAdapter(Model.class, new Model.JsonAdapter())
            .registerTypeAdapter(Construction.class, new Construction.JsonAdapter())
            .registerTypeAdapter(ModelConfig.class, new ModelConfig.JsonAdapter())
            .registerTypeAdapter(Tariff.class, new Tariff.JsonAdapter())
            .create();
    public static final File DIR_DEFAULT = new File(".");
    public static final FileChooser.ExtensionFilter[] EXTENSION_FILTERS_DEFAULT = new FileChooser.ExtensionFilter[]{
            new FileChooser.ExtensionFilter("JSON files", "*.json"),
            new FileChooser.ExtensionFilter("All files", "*")
    };
    //EnumCollector
    public static final EnumCollector<IConstructionType> ENUM_COLLECTOR_CONSTRUCTIONS = new EnumCollector<>();
    //Time
    public static final DecimalFormat FORMATTER_LONG = new DecimalFormat("00");
    //60 sec = %rate% minutes
    public static double TIME_RATE_SEC_TO_MIN = 2;
    public static long TIME_BEFORE_TIME_TYPE_SWITCH_SEC = 360;
    public static TimeType TIME_TYPE_DEFAULT = TimeType.DAY;
}