package ru.ssau.parkingsimulation.util;

public interface IExtendableEnum {
    int extOrdinal();

    String name();
}