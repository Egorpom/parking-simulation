package ru.ssau.parkingsimulation.config;

import javafx.scene.control.TreeItem;
import javafx.scene.layout.VBox;
import ru.ssau.parkingsimulation.util.Constants;
import ru.ssau.parkingsimulation.view.INode;
import ru.ssau.parkingsimulation.view.ITreeItem;

import java.io.Serializable;
import java.util.Objects;

public class Configuration implements ITreeItem<Configuration>, INode, Serializable, Cloneable {
    protected transient String title;
    protected transient TreeItem<Configuration> treeItem;
    protected transient VBox node;
    protected transient Configuration source = null;

    public Configuration() {
        this("");
    }

    public Configuration(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public boolean isValid() {
        return true;
    }

    public Configuration getSource() {
        return source;
    }

    @Override
    public String toString() {
        return title;
    }

    @Override
    public TreeItem<Configuration> getTreeItem() {
        if (treeItem == null) {
            buildTreeItem();
        }
        return treeItem;
    }

    @Override
    public void buildTreeItem() {
        treeItem = new TreeItem<>(this);
    }

    @Override
    public VBox getNode() {
        if (node == null) {
            buildNode();
        }
        return node;
    }

    @Override
    public void buildNode() {
        node = new VBox();
        node.setPadding(Constants.INSETS_DEFAULT);
    }

    public void updateSource() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Configuration)) return false;
        Configuration that = (Configuration) o;
        return Objects.equals(title, that.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title);
    }

    @Override
    public Configuration clone() {
        Configuration configuration = null;
        try {
            configuration = (Configuration) super.clone();
            configuration.source = this;
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return configuration;
    }
}