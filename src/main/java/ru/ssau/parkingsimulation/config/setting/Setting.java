package ru.ssau.parkingsimulation.config.setting;

import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Pos;
import javafx.scene.layout.HBox;
import ru.ssau.parkingsimulation.util.Constants;
import ru.ssau.parkingsimulation.view.INode;

import java.io.Serializable;
import java.util.Objects;

public abstract class Setting<T> implements INode, Serializable, Cloneable {
    protected String title;
    protected boolean locked;
    protected transient SimpleObjectProperty<T> observable;
    protected transient HBox node;

    public Setting(String title) {
        this.title = title;
    }

    public Setting(String title, T value) {
        this(title, value, false);
    }

    public Setting(String title, T value, boolean locked) {
        this.title = title;
        observable = new SimpleObjectProperty<>(value);
        this.locked = locked;
    }

    public String getTitle() {
        return title;
    }

    public T getValue() {
        return observable.getValue();
    }

    public void setValue(T value) {
        observable.setValue(value);
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public SimpleObjectProperty<T> observableProperty() {
        return observable;
    }

    @Override
    public HBox getNode() {
        if (node == null) {
            buildNode();
        }
        return node;
    }

    @Override
    public void buildNode() {
        node = new HBox();
        node.setSpacing(10);
        node.setPadding(Constants.INSETS_DEFAULT);
        node.setAlignment(Pos.CENTER_LEFT);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Setting)) return false;
        Setting<?> setting = (Setting<?>) o;
        return title.equals(setting.title) && observable.getValue().equals(setting.observable.getValue());
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, observable.getValue());
    }

    @Override
    public Setting<T> clone() {
        Setting<T> setting = null;
        try {
            setting = (Setting<T>) super.clone();
            setting.observable = new SimpleObjectProperty<>(observable.getValue());
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return setting;
    }
}