package ru.ssau.parkingsimulation.config.setting;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.util.converter.DoubleStringConverter;

public class DoubleSetting extends Setting<Double> {
    public DoubleSetting(String title) {
        super(title, 0.0D);
    }

    public DoubleSetting(String title, Double value) {
        super(title, value);
    }

    public DoubleSetting(String title, Double value, boolean locked) {
        super(title, value, locked);
    }

    @Override
    public void buildNode() {
        super.buildNode();
        TextField textField = new TextField();
        textField.setDisable(locked);
        TextFormatter<Double> formatter = new TextFormatter<>(new DoubleStringConverter());
        formatter.valueProperty().bindBidirectional(observable);
        textField.setTextFormatter(formatter);
        node.getChildren().add(textField);
        node.getChildren().add(new Label(title));
    }
}