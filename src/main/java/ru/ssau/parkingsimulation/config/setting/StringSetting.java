package ru.ssau.parkingsimulation.config.setting;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class StringSetting extends Setting<String> {
    public StringSetting(String title) {
        super(title, "");
    }

    public StringSetting(String title, String value) {
        super(title, value);
    }

    public StringSetting(String title, String value, boolean locked) {
        super(title, value, locked);
    }

    @Override
    public void buildNode() {
        super.buildNode();
        TextField textField = new TextField();
        textField.setDisable(locked);
        textField.textProperty().bindBidirectional(observable);
        node.getChildren().add(textField);
        node.getChildren().add(new Label(title));
    }
}