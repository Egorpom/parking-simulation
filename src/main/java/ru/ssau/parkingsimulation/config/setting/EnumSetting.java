package ru.ssau.parkingsimulation.config.setting;

import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;

public class EnumSetting<T extends Enum<?>> extends Setting<T> {
    protected T[] values;

    public EnumSetting(String title, T[] values) {
        this(title, values, values[0]);
    }

    public EnumSetting(String title, T[] values, T defaultValue) {
        super(title, defaultValue);
        this.values = values;
    }

    public EnumSetting(String title, T[] values, T defaultValue, boolean locked) {
        super(title, defaultValue);
        this.values = values;
        this.locked = locked;
    }

    @Override
    public void buildNode() {
        super.buildNode();
        ChoiceBox<T> choiceBox = new ChoiceBox<>();
        choiceBox.setDisable(locked);
        choiceBox.getItems().addAll(values);
        choiceBox.valueProperty().bindBidirectional(observable);
        node.getChildren().add(choiceBox);
        node.getChildren().add(new Label(title));
    }
}