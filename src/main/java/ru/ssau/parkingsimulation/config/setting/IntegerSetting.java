package ru.ssau.parkingsimulation.config.setting;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.util.converter.IntegerStringConverter;

public class IntegerSetting extends Setting<Integer> {
    public IntegerSetting(String title) {
        super(title, 0);
    }

    public IntegerSetting(String title, Integer value) {
        super(title, value);
    }

    public IntegerSetting(String title, Integer value, boolean locked) {
        super(title, value, locked);
    }

    @Override
    public void buildNode() {
        super.buildNode();
        TextField textField = new TextField();
        textField.setDisable(locked);
        TextFormatter<Integer> formatter = new TextFormatter<>(new IntegerStringConverter());
        formatter.valueProperty().bindBidirectional(observable);
        textField.setTextFormatter(formatter);
        node.getChildren().add(textField);
        node.getChildren().add(new Label(title));
    }
}