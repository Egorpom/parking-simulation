package ru.ssau.parkingsimulation.config.setting;

import javafx.scene.control.CheckBox;

public class BooleanSetting extends Setting<Boolean> {
    public BooleanSetting(String title) {
        super(title, false);
    }

    public BooleanSetting(String title, Boolean value) {
        super(title, value);
    }

    public BooleanSetting(String title, Boolean value, boolean locked) {
        super(title, value, locked);
    }

    @Override
    public void buildNode() {
        super.buildNode();
        CheckBox checkBox = new CheckBox(title);
        checkBox.setDisable(locked);
        checkBox.selectedProperty().bindBidirectional(observable);
    }
}